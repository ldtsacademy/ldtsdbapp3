package es.ldts.dbapp3;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


class db {

    public static SQLiteDatabase datafile;

    public static void open() {
        boolean ok = false;
        try { ok = datafile.isOpen(); }
        catch (Exception e) { }
        if (!ok)
            datafile = SQLiteDatabase.openDatabase(filemanager.cacheaddresslocal + "/data/data.db", null, SQLiteDatabase.OPEN_READONLY);
    }

    public static boolean isOpen() {
        boolean opened = false;
        try {
            opened = datafile.isOpen();
        } catch (Exception e) { }
        return opened;
    }

    public static String[][] select(String sql) {

        open();

        if (datafile.isOpen()) {
            System.out.println("Esto va-------");
        }

        Cursor cursor = datafile.rawQuery(sql,null);
        String[][] result = new String[cursor.getCount()][cursor.getColumnCount()];
        int f = 0;
        while (cursor.moveToNext()){
            for (int i=0 ; i < cursor.getColumnCount(); i++) {
                result[f][i] = cursor.getString(i);
            }
            f++;
        }
        cursor.close();

        return result;

// since we have a named column we can do
    }

    public static String datosql(String sql) {
        String[][] result = select(sql);
        String valor = "";

        try {
            valor = result[0][0];
        } catch (IndexOutOfBoundsException e) {  }

        return valor;
    }

}
