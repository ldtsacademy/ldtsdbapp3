package es.ldts.dbapp3;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class utiles {


    public static Date cleandate(Date fecha) {
        Date fechaok =
                string2dateus(
                        dateus2string(fecha));
        return fechaok;
    }
    public static Date string2dateus(String fecha) {
        Calendar result = Calendar.getInstance();
        String[] datosf = fecha.split("-");
        int year = Integer.parseInt(datosf[0]);
        int month = Integer.parseInt(datosf[1]) - 1;
        int day = Integer.parseInt(datosf[2]);
        result.set(year, month, day);
        return result.getTime();
    }
    public static String dateus2string(Date fechadate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(fechadate);
        return format;
    }

    public static int IntDiv(int num, int dvsr) {

        // performs integer division of num/dvsr - eg IntDiv(9,4)=2
        boolean negate = false;
        int result = 0;
        int num2 = num;
        int dvsr2 = dvsr;

        if (dvsr == 0) {
            return -1;
        }
        else {
            if (num * dvsr < 0 ) {
                negate = true;
            }
            if (num < 0) {
                num2 = -num;
            }
            if (dvsr < 0) {
                dvsr2 = -dvsr;
            }
            result = ((num2 - (num2 % dvsr2)) / dvsr2);
            if (negate) {
                return -result;
            } else {
                return result;
            }
        }
    }

    public static int getDayOfWeek(Date fecha) {
        int result;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        result = calendar.get(Calendar.DAY_OF_WEEK);
        //Domingo 1, Sabado 7 -- cambiamos a la española: Lunes 1, Domingo 7
        result-=1;
        if (result==0) {
            result = 7;
        }
        return result;
    }

    public static int nSemana(Date desde, Date hasta) {
        return (1+IntDiv(datediff(desde, hasta), 7));
    }

    public static int nSemana(String desde, Date hasta) {
        Date fecha0 = string2dateus(desde);
        return nSemana(fecha0, hasta);
    }

    public static int str2int(String numero) {
        int result = 0;
        try {
            result = Integer.parseInt(numero);
        } catch (NumberFormatException e) { }
        return result;
    }

// added for android

    public static Date adddaystodate(int days, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }

    public static int datediff(Date desde, Date hasta) {
        int result = 0;
        Calendar d = Calendar.getInstance();
        Calendar h = Calendar.getInstance();
        d.setTime(desde);
        h.setTime(hasta);
        d.set(Calendar.HOUR, 0);
        d.set(Calendar.MINUTE, 0);
        d.set(Calendar.SECOND,0);
        d.set(Calendar.MILLISECOND,0);
        h.set(Calendar.HOUR, 3); //NECESARIO AL MENOS 1 PARA EVITAR PROBLEMAS CON CAMBIO DE HORARIO EN PRIMAVERA Y OTOÑO
        h.set(Calendar.MINUTE, 0);
        h.set(Calendar.SECOND,0);
        h.set(Calendar.MILLISECOND,0);

        long diff = h.getTime().getTime() - d.getTime().getTime();
        result = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        if (result==0) {
            result = (int) ((TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS)/24.0)-0.22);
            //Esta linea garantiza que en caso de cambio de hora primavera/otonio 19 horas se entienda como un dia.
            //Se restan 5 por 1 de cambio horario + 4 de margen por las 3 que se sumaron. (0.22 + 19/24 > 1)
            //Manda narices que java no cuente un dia si hay cambio horario. Viva SQL
        }

        return result;
    }


}
